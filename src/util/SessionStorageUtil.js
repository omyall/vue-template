'use strict'

export function SessionSet (key = '', val = {}) {
  if (val) sessionStorage.setItem(key, JSON.stringify(val))
}
export function SessionGet (key = '') {
  const val = sessionStorage.getItem(key)
  if (val) return JSON.parse(val)
  else return {}
}
export function SessionDel (key = '') {
  sessionStorage.removeItem(key)
}
export function SessionGroupSet (key, val, parentKey = null) {
  parentKey = (parentKey === null) ? window.location.host + window.location.pathname : parentKey
  let obj = SessionGet(parentKey)
  if (!obj) {
    obj = {}
  }
  obj[key] = val
  SessionSet(parentKey, obj)
}
export function SessionGroupGet (key, parentKey = null) {
  parentKey = (parentKey === null) ? window.location.host + window.location.pathname : parentKey
  const obj = SessionGet(parentKey)
  return (obj) ? obj[key] : ''
}
export function SessionGroupDel (key, parentKey = null) {
  parentKey = (parentKey === null) ? window.location.host + window.location.pathname : parentKey
  const obj = SessionGet(parentKey)
  if (obj) {
    delete obj[key]
    SessionSet(parentKey, obj)
  }
}
